import { Formik, Form, Field, ErrorMessage } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import './Form.css'
import { SuccessMessage } from '../Success/SuccessMessage';
import * as Yup from 'yup';
import { AppState, Application } from '../../utils';


const initialApplicationState: Application = {
  name: '',
  phone: '',
  email: '',
  date: new Date(),
};

const FormValidationSchema = Yup.object().shape({
  name: Yup.string().required('Пожалуйста, введите имя'),
  phone: Yup.string().matches(/^\+7\d{10}$/, 'Пожалуйста, введите корректный номер телефона'),
  email: Yup.string().email('Пожалуйста, введите корректный адрес электронной почты').required('Пожалуйста, введите адрес электронной почты'),
});

const ApplicationForm = () => {
  const success = useSelector((state: AppState) => state.success);
  const dispatch = useDispatch()
  const handleSubmit = (values: Application) => {
    console.log(success)
    const newApplication: Application = {
      ...values,
      date: new Date(),
    };
    dispatch({ type: 'ADD_APPLICATION', payload: newApplication });
    dispatch({ type: 'SET_SUCCESS', payload: true });
  };

  return ( 
  <div>
    {success ? (
    <SuccessMessage/>
  ) :
    (<Formik initialValues={initialApplicationState} validationSchema={FormValidationSchema} onSubmit={handleSubmit}>
      {({ values, errors, handleChange, handleBlur, isValid, dirty }) => (
        <div className='form'>
         <h2>Оставить заявку</h2>
          <Form>
              <Field 
                type="text" 
                name="name" 
                value={values.name} 
                onChange={handleChange} 
                onBlur={handleBlur} 
                placeholder="Ваше имя" 
                component="input"  
                className={errors.name  ? 'errors' : ''}/>
              <br />
              <ErrorMessage name="name"  className="error" component="div"/>
              <Field 
                type="tel" 
                name="phone" 
                value={values.phone} 
                onChange={handleChange} 
                onBlur={handleBlur} 
                placeholder="Номер телефона"  
                className={errors.phone  ? 'errors' : ''} />
              <br />
              <ErrorMessage name="phone"  className="error" component="div"/>
              <Field 
                type="email" 
                name="email" 
                value={values.email} 
                onChange={handleChange} 
                onBlur={handleBlur} 
                placeholder="E-mail" 
                className={errors.email  ? 'errors' : ''}/>
              <br />
              <ErrorMessage name="email" className="error"  component="div"/>

              <button className='button' type="submit" disabled={!isValid || !dirty}>
                Отправить
              </button>
            </Form>
          </div>
      )}
    </Formik>) }
  </div>

  );
};

export default ApplicationForm;

