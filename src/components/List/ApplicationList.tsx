import { useSelector } from 'react-redux';
import { useState } from 'react';
import './List.css'
import Arrow  from '../../assets/arrow.svg'; 
import { AppState, Application } from '../../utils';

const ApplicationList = () => {
  const [sortBy, setSortBy] = useState<'asc' | 'desc'>('asc');
  const applications = useSelector((state: AppState) => state.applications);

  // Функция сравнения для сортировки заявок по дате
  const compareByDate = (a: Application, b: Application) => {
    if (sortBy === 'asc') {
      return a.date.getTime() - b.date.getTime();
    } else {
      return b.date.getTime() - a.date.getTime();
    }
  };

  // Сортировка заявок по дате
  const sortedApplications = [...applications].sort(compareByDate);

  // Обработчик клика на кнопку сортировки
  const handleSortClick = () => {
    setSortBy(sortBy === 'asc' ? 'desc' : 'asc');
  };

  return (
    <div className='list'>
      <h2>Все заявки</h2>
      <div className='title'>
        <p>№</p>
        <p>Имя</p>
        <p>Номер телефона</p>
        <p>E-mail</p>
        <div className='sort'>
          Дата добавления  
          <button onClick={handleSortClick}>
          <img src={Arrow} alt="Arrow" />
          </button>
        </div>
      </div>
      {sortedApplications.length === 0 ? (
        <p className='notfound'>Нет заявок</p>
      ) : (
        <ul>
          {sortedApplications.map((application: Application, index: number) => (
            <li key={index}>
              <p>{index + 1}</p>
              <p>{application.name}</p>
              <p> {application.phone}</p>
              <p> {application.email}</p>
              <span>{application.date.toDateString()}</span>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default ApplicationList;


