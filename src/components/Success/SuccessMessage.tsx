import React from 'react'
import { useDispatch } from 'react-redux';
import './Success.css'
export const SuccessMessage = () => {
  const dispatch = useDispatch();

  const handleReset = () => {
    dispatch({ type: 'RESET_SUCCESS' });
  };
  return (
    <div className='Success'>
      <h2>Заявка успешно отправлена!</h2>
        <p>Мы свяжемся с вами в ближайшее время</p> 
      <button onClick={handleReset}>Рестарт формы</button>
    </div>
  )
}
