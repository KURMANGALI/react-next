import React from 'react'
import { NavLink } from 'react-router-dom'
import './Header.css'

export const Header = () => {
  return (
    <div className='header'>
      <NavLink
        to="/"
      >
        Оставить заявку
      </NavLink>
      <NavLink
        to="/list"
      >
      Список заявок
      </NavLink>
    </div>
  )
}
