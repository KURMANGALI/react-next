export interface AppState {
    applications: Application[];
    success: boolean;
 }

export interface Application {
    name: string;
    phone: string;
    email: string;
    date: Date;
}

export const ADD_APPLICATION = 'ADD_APPLICATION'
export const RESET_SUCCESS = 'RESET_SUCCESS'