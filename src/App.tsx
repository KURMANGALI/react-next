import React from 'react';
import { NavLink, Route, Routes } from 'react-router-dom';
import ApplicationForm from './components/Form/ApplicationForm';
import { Header } from './components/Header/Header';
import './App.css'
import ApplicationList from './components/List/ApplicationList';

const App = () => {
  return (
    <div className='App'>
    <Header />
    <Routes>
      <Route path="/" element={<ApplicationForm />} />
      <Route path="/list" element={<ApplicationList />} />
    </Routes>
    </div>
  );
};

export default App;
