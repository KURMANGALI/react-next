import { createStore } from 'redux';
import { ADD_APPLICATION, AppState, RESET_SUCCESS } from '../utils';



const initialState: AppState = {
  applications: [],
  success: false,
};

function rootReducer(state = initialState, action: { type: any; payload: any; } ) {
  switch (action.type) {
    case ADD_APPLICATION:
      return {
        ...state,
        applications: [...state.applications, action.payload],
        success: true,
      };
    case RESET_SUCCESS:
      return {
        ...state,
        success: false,
      };
    default:
      return state;
  }
}

const store = createStore(rootReducer);

export default store;
